using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Notifications;
using Avalonia.Controls.Primitives;
using Decrypt.Models;
using Decrypt.Services.Implementation;
using Decrypt.ViewModels;

namespace Decrypt.Views;

public partial class MainPage : Window {
    /// <summary>
    /// Avalonia弹窗
    /// </summary>
    private static WindowNotificationManager? _manager;

    public MainPage() {
        InitializeComponent();
        //以下非纯MVVM绑定方式，请勿参考
        //TODO 将来的某一天替换成纯MVVM绑定
        DataContext =
            (Application.Current.Resources["ViewModelLocator"] as
                ViewModelLocator).MainPageViewModel;
        Loaded += (sender, e) => {
            (DataContext as MainPageViewModel)?.PageAppearingCommandFunction();
        };
        InitUI();
    }

    /// <summary>
    /// 重写方法，为了使用弹窗
    /// </summary>
    /// <param name="e">事件参数</param>
    protected override void OnApplyTemplate(TemplateAppliedEventArgs e) {
        base.OnApplyTemplate(e);
        _manager = new WindowNotificationManager(this) { MaxItems = 3 };
    }

    /// <summary>
    /// 在当前页发起弹窗
    /// </summary>
    /// <param name="notificationType">通知类型</param>
    /// <param name="title">弹窗标题</param>
    /// <param name="message">弹窗内容</param>
    public static void ShowPopup(NotificationType notificationType,
        string title, string message) {
        _manager?.Show(new Notification(title, message, notificationType));
    }

    /// <summary>
    /// 初始化UI
    /// </summary>
    private void InitUI() {
        //添加带包版本号的窗口标题
        Title = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName()
            .Version?.ToString()}";
    }
}