﻿using GalaSoft.MvvmLight.Ioc;
using Decrypt.Services.Implementation;
using Decrypt.Services.IServices;

namespace Decrypt.ViewModels;

/// <summary>
/// ViewModel Locator
/// </summary>
public class ViewModelLocator {
    /// <summary>
    /// 主页ViewModel
    /// </summary>
    public MainPageViewModel MainPageViewModel =>
        SimpleIoc.Default.GetInstance<MainPageViewModel>();

    /// <summary>
    /// 构造函数
    /// </summary>
    public ViewModelLocator() {
        SimpleIoc.Default.Register<ILogService, LogService>();
        SimpleIoc.Default.Register<IDecryptService, DecryptService>();
        SimpleIoc.Default.Register<MainPageViewModel>();
    }
}