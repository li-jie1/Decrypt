﻿using System;
using System.Threading.Tasks;

namespace Decrypt.Services.IServices;

public interface IDecryptService {
    event EventHandler<DecryptEventArgs> Decrypt;

    Task<bool> DecryptOneAsync(string encryptedFilePath, string encryptedFileName);

    Task<bool> DecryptDirectoryAsync(string directory);
}

public class DecryptEventArgs:EventArgs {
    /// <summary>
    /// 文件名
    /// </summary>
    public string FileName { get; set; } = string.Empty;

    /// <summary>
    /// 解密是否成功
    /// </summary>
    public bool DecryptSuccessful { get; set; }
}