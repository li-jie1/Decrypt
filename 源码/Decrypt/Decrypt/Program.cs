﻿using System;
using System.IO;
using System.Threading;
using Avalonia;
using Decrypt.Services.Implementation;

namespace Decrypt;

public class Program {
    /// <summary>
    /// 防止进程多开的变量
    /// </summary>
    private static bool _createdNew;

    /// <summary>
    /// 用于防止进程多开
    /// </summary>
    /// <remarks>name参数采用的是一个guid</remarks>
    private static Mutex mutex = new(true,
        $"Global\\{nameof(Decrypt)}_EB0A91C54B004722850D2ED09ED41E2C"/*TODO 改变前面的UUID*/, out _createdNew);

    // Initialization code. Don't use any Avalonia, third-party APIs or any
    // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
    // yet and stuff might break.
    [STAThread]
    public static void Main(string[] args) {
        //重复进程检查
        if (!_createdNew) return;
        BuildAvaloniaApp().StartWithClassicDesktopLifetime(args);
    }

    // Avalonia configuration, don't remove; also used by visual designer.
    public static AppBuilder BuildAvaloniaApp() {
        return AppBuilder.Configure<App>().UsePlatformDetect().WithInterFont()
            .LogToTrace();
    }
}